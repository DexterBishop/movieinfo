//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2014.09.28 um 06:18:23 PM CEST 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="year" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="outline" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plot" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="runtime" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="playcount" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="fileinfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="streamdetails">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="audio" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="channels" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                       &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="subtitle" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="video">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="aspect" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                                       &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="durationinseconds" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                       &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                       &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                       &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="multiView_Count" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="encodedSettings" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "title",
    "year",
    "outline",
    "plot",
    "runtime",
    "playcount",
    "fileinfo"
})
@XmlRootElement(name = "movie")
public class Movie {

    @XmlElement(required = true)
    protected String title;
    protected short year;
    @XmlElement(required = true)
    protected String outline;
    @XmlElement(required = true)
    protected String plot;
    protected byte runtime;
    protected byte playcount;
    @XmlElement(required = true)
    protected Movie.Fileinfo fileinfo;

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der year-Eigenschaft ab.
     * 
     */
    public short getYear() {
        return year;
    }

    /**
     * Legt den Wert der year-Eigenschaft fest.
     * 
     */
    public void setYear(short value) {
        this.year = value;
    }

    /**
     * Ruft den Wert der outline-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutline() {
        return outline;
    }

    /**
     * Legt den Wert der outline-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutline(String value) {
        this.outline = value;
    }

    /**
     * Ruft den Wert der plot-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlot() {
        return plot;
    }

    /**
     * Legt den Wert der plot-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlot(String value) {
        this.plot = value;
    }

    /**
     * Ruft den Wert der runtime-Eigenschaft ab.
     * 
     */
    public byte getRuntime() {
        return runtime;
    }

    /**
     * Legt den Wert der runtime-Eigenschaft fest.
     * 
     */
    public void setRuntime(byte value) {
        this.runtime = value;
    }

    /**
     * Ruft den Wert der playcount-Eigenschaft ab.
     * 
     */
    public byte getPlaycount() {
        return playcount;
    }

    /**
     * Legt den Wert der playcount-Eigenschaft fest.
     * 
     */
    public void setPlaycount(byte value) {
        this.playcount = value;
    }

    /**
     * Ruft den Wert der fileinfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Movie.Fileinfo }
     *     
     */
    public Movie.Fileinfo getFileinfo() {
        return fileinfo;
    }

    /**
     * Legt den Wert der fileinfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Movie.Fileinfo }
     *     
     */
    public void setFileinfo(Movie.Fileinfo value) {
        this.fileinfo = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="streamdetails">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="audio" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="channels" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                             &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="subtitle" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="video">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="aspect" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                             &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="durationinseconds" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                             &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                             &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                             &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="multiView_Count" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="encodedSettings" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "streamdetails"
    })
    public static class Fileinfo {

        @XmlElement(required = true)
        protected Movie.Fileinfo.Streamdetails streamdetails;

        /**
         * Ruft den Wert der streamdetails-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Movie.Fileinfo.Streamdetails }
         *     
         */
        public Movie.Fileinfo.Streamdetails getStreamdetails() {
            return streamdetails;
        }

        /**
         * Legt den Wert der streamdetails-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Movie.Fileinfo.Streamdetails }
         *     
         */
        public void setStreamdetails(Movie.Fileinfo.Streamdetails value) {
            this.streamdetails = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="audio" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="channels" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                   &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="subtitle" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="video">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="aspect" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                   &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="durationinseconds" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                   &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                   &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                   &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="multiView_Count" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="encodedSettings" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "audio",
            "subtitle",
            "video"
        })
        public static class Streamdetails {

            protected List<Movie.Fileinfo.Streamdetails.Audio> audio;
            protected List<Movie.Fileinfo.Streamdetails.Subtitle> subtitle;
            @XmlElement(required = true)
            protected Movie.Fileinfo.Streamdetails.Video video;

            /**
             * Gets the value of the audio property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the audio property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAudio().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Movie.Fileinfo.Streamdetails.Audio }
             * 
             * 
             */
            public List<Movie.Fileinfo.Streamdetails.Audio> getAudio() {
                if (audio == null) {
                    audio = new ArrayList<Movie.Fileinfo.Streamdetails.Audio>();
                }
                return this.audio;
            }

            /**
             * Gets the value of the subtitle property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the subtitle property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSubtitle().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Movie.Fileinfo.Streamdetails.Subtitle }
             * 
             * 
             */
            public List<Movie.Fileinfo.Streamdetails.Subtitle> getSubtitle() {
                if (subtitle == null) {
                    subtitle = new ArrayList<Movie.Fileinfo.Streamdetails.Subtitle>();
                }
                return this.subtitle;
            }

            /**
             * Ruft den Wert der video-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Movie.Fileinfo.Streamdetails.Video }
             *     
             */
            public Movie.Fileinfo.Streamdetails.Video getVideo() {
                return video;
            }

            /**
             * Legt den Wert der video-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Movie.Fileinfo.Streamdetails.Video }
             *     
             */
            public void setVideo(Movie.Fileinfo.Streamdetails.Video value) {
                this.video = value;
            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="channels" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *         &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "channels",
                "codec",
                "language",
                "bitrate"
            })
            public static class Audio {

                protected byte channels;
                @XmlElement(required = true)
                protected String codec;
                @XmlElement(required = true)
                protected String language;
                @XmlElement(required = true)
                protected String bitrate;

                /**
                 * Ruft den Wert der channels-Eigenschaft ab.
                 * 
                 */
                public byte getChannels() {
                    return channels;
                }

                /**
                 * Legt den Wert der channels-Eigenschaft fest.
                 * 
                 */
                public void setChannels(byte value) {
                    this.channels = value;
                }

                /**
                 * Ruft den Wert der codec-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodec() {
                    return codec;
                }

                /**
                 * Legt den Wert der codec-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodec(String value) {
                    this.codec = value;
                }

                /**
                 * Ruft den Wert der language-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLanguage() {
                    return language;
                }

                /**
                 * Legt den Wert der language-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLanguage(String value) {
                    this.language = value;
                }

                /**
                 * Ruft den Wert der bitrate-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBitrate() {
                    return bitrate;
                }

                /**
                 * Legt den Wert der bitrate-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBitrate(String value) {
                    this.bitrate = value;
                }

            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "language"
            })
            public static class Subtitle {

                @XmlElement(required = true)
                protected String language;

                /**
                 * Ruft den Wert der language-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLanguage() {
                    return language;
                }

                /**
                 * Legt den Wert der language-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLanguage(String value) {
                    this.language = value;
                }

            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="aspect" type="{http://www.w3.org/2001/XMLSchema}float"/>
             *         &lt;element name="codec" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="durationinseconds" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *         &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *         &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *         &lt;element name="bitrate" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="multiView_Count" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="encodedSettings" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "aspect",
                "codec",
                "durationinseconds",
                "height",
                "width",
                "bitrate",
                "multiViewCount",
                "encodedSettings"
            })
            public static class Video {

                protected float aspect;
                @XmlElement(required = true)
                protected String codec;
                protected short durationinseconds;
                protected short height;
                protected short width;
                @XmlElement(required = true)
                protected String bitrate;
                @XmlElement(name = "multiView_Count", required = true)
                protected String multiViewCount;
                @XmlElement(required = true)
                protected String encodedSettings;

                /**
                 * Ruft den Wert der aspect-Eigenschaft ab.
                 * 
                 */
                public float getAspect() {
                    return aspect;
                }

                /**
                 * Legt den Wert der aspect-Eigenschaft fest.
                 * 
                 */
                public void setAspect(float value) {
                    this.aspect = value;
                }

                /**
                 * Ruft den Wert der codec-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodec() {
                    return codec;
                }

                /**
                 * Legt den Wert der codec-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodec(String value) {
                    this.codec = value;
                }

                /**
                 * Ruft den Wert der durationinseconds-Eigenschaft ab.
                 * 
                 */
                public short getDurationinseconds() {
                    return durationinseconds;
                }

                /**
                 * Legt den Wert der durationinseconds-Eigenschaft fest.
                 * 
                 */
                public void setDurationinseconds(short value) {
                    this.durationinseconds = value;
                }

                /**
                 * Ruft den Wert der height-Eigenschaft ab.
                 * 
                 */
                public short getHeight() {
                    return height;
                }

                /**
                 * Legt den Wert der height-Eigenschaft fest.
                 * 
                 */
                public void setHeight(short value) {
                    this.height = value;
                }

                /**
                 * Ruft den Wert der width-Eigenschaft ab.
                 * 
                 */
                public short getWidth() {
                    return width;
                }

                /**
                 * Legt den Wert der width-Eigenschaft fest.
                 * 
                 */
                public void setWidth(short value) {
                    this.width = value;
                }

                /**
                 * Ruft den Wert der bitrate-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBitrate() {
                    return bitrate;
                }

                /**
                 * Legt den Wert der bitrate-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBitrate(String value) {
                    this.bitrate = value;
                }

                /**
                 * Ruft den Wert der multiViewCount-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMultiViewCount() {
                    return multiViewCount;
                }

                /**
                 * Legt den Wert der multiViewCount-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMultiViewCount(String value) {
                    this.multiViewCount = value;
                }

                /**
                 * Ruft den Wert der encodedSettings-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEncodedSettings() {
                    return encodedSettings;
                }

                /**
                 * Legt den Wert der encodedSettings-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEncodedSettings(String value) {
                    this.encodedSettings = value;
                }

            }

        }

    }

}
