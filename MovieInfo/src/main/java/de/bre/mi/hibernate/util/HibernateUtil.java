package de.bre.mi.hibernate.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class HibernateUtil {
	private static final Logger LOG = LoggerFactory.getLogger(HibernateUtil.class);
	private static final SessionFactory SESSION_FACTORY = buildSessionFactory();

	private HibernateUtil() {}

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration configuration = new Configuration();
			configuration.configure();
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

			// Use hibernate.cfg.xml to get a SessionFactory
			return new Configuration().configure().buildSessionFactory(serviceRegistry);
		} catch (Throwable e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("SessionFactory creation failed." + e);
			}
			throw new ExceptionInInitializerError(e);
		}
	}

	/**
	 * returns the SessionFactory object
	 * 
	 * @return the SessionFactory
	 */

	public static SessionFactory getSessionFactory() {
		return SESSION_FACTORY;
	}

	/**
	 * destroy the SessionFactory
	 */
	public static void shutdown() {
		getSessionFactory().close();
	}
}
