package de.bre.mi.view;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bre.mi.model.Movie;

public class MovieLayoutController {
	private static final Logger LOG = LoggerFactory.getLogger(MovieLayoutController.class);

	private Movie movie;
	private MainLayoutController mlc;

	@FXML
	private Rectangle boarder;
	@FXML
	private Label titleLbl;
	@FXML
	private ImageView imageView;
	@FXML
	private Button addRemoveBtn;

	/**
	 * must be called after setMovie
	 */
	public void setImage() {
		if (movie != null) {
			titleLbl.setText(movie.getTitle());
			if (Files.exists(Paths.get(movie.getPath() + "\\" + movie.getFileName() + "-poster.jpg"), new LinkOption[] {})) {
				imageView.setImage(new Image("file:" + movie.getPath() + "\\" + movie.getFileName() + "-poster.jpg", 140, 210, true, true,
						true));
			} else {
				imageView.setVisible(false);
				titleLbl.setVisible(true);
			}
		} else {
			if (LOG.isWarnEnabled()) {
				LOG.warn("coudn't set image. movie is NULL.");
			}
		}
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	@FXML
	private void handleCLickImage() { // NOPMD by dbg on 27.09.14 14:35
		mlc.setMovieNameLbl(movie.getTitle(), movie.getYear(), movie.getOutline(), movie.getPlot());
	}
	
	@FXML
	private void handleAddRemove() { // NOPMD by dbg on 27.09.14 14:35
		String path = movie.getPath() + "\\" + movie.getFileName() + ".mkv";
		if (addRemoveBtn.getText().equals("add")) {
			boarder.setVisible(true);
			addRemoveBtn.setText("remove");
			mlc.addMovie(path, movie.getFileSize());
			if (LOG.isDebugEnabled()) {
				LOG.debug("add: " + movie.getFileName());
			}
		} else if (addRemoveBtn.getText().equals("remove")) {
			boarder.setVisible(false);
			addRemoveBtn.setText("add");
			mlc.removeMovie(path, movie.getFileSize());
			if (LOG.isDebugEnabled()) {
				LOG.debug("rem: " + movie.getFileName());
			}
		}
	}

	/**
	 * set the MainLayoutController
	 * 
	 * @param mlc
	 *            the MainLayoutController
	 */
	public void setMainLayoutController(MainLayoutController mlc) {
		this.mlc = mlc;
	}
}
