package de.bre.mi.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.nio.file.ExtendedCopyOption;

import de.bre.mi.Main;
import de.bre.mi.hibernate.util.HibernateUtil;
import de.bre.mi.model.Movie;
import de.bre.mi.util.LoadMoviesRunnable;
import de.bre.mi.util.PreferencesStore;

public class MainLayoutController {
	@FXML
	private Label updateLbl;
	@FXML
	private Label movieCountLbl;
	@FXML
	private Label selMovieCountLbl;
	@FXML
	private Label selMovieCountText;
	@FXML
	private Label selMovieSizeLbl;
	@FXML
	private Label selMovieSizeText;
	@FXML
	private Label movieTitleLbl;
	@FXML
	private Label movieYearLbl;

	@FXML
	private TextFlow outlineTxtFlow;
	@FXML
	private TextFlow plotTxtFlow;

	@FXML
	private VBox movieInfoBox;

	@FXML
	private ScrollPane movieTab;

	@FXML
	private Button updateBtn;
	@FXML
	private Button resetBtn;
	@FXML
	private Button copyBtn;

	@FXML
	private ProgressBar loadProgress = new ProgressBar();

	@FXML
	private GridPane gridPane;

	private static final Logger LOG = LoggerFactory.getLogger(MainLayoutController.class);
	private static final String[] PATHS = { "W:\\HD-Filme", "X:\\HD-Filme", "Y:\\HD-Filme", "Z:\\HD-Filme" };

	private Main mainApp;
	private CopyDialogController cdc;
	private Set<String> importMovies = new TreeSet<>();
	private Set<Movie> movies = new TreeSet<>();
	private Set<String> copySet = new TreeSet<>();

	@FXML
	private void handleUpdate() {
		Thread thread = new Thread("load nfo's") {
			@Override
			public void run() {
				if (LOG.isInfoEnabled()) {
					LOG.info("update START");
				}
				List<Thread> threadList = new ArrayList<Thread>();
				Thread thread;

				Platform.runLater(() -> {
					setUpdateBtnDisable(true);
				});

				for (String path : PATHS) {
					thread = new Thread(new LoadMoviesRunnable(path, movies), "read nfo " + path);
					thread.setDaemon(true);
					threadList.add(thread);
					thread.start();
				}

				for (Thread t : threadList) {
					try {
						t.join();
					} catch (InterruptedException e) {
						if (LOG.isErrorEnabled()) {
							LOG.error("coudn't join thread", e);
						}
					}
				}

				PreferencesStore.setLastCheck(LocalDateTime.now().format(
						DateTimeFormatter.ofPattern("EEE, d LLL yyyy HH:mm:ss", Locale.GERMAN)));

				new Thread("save to sqldb") {
					@Override
					public void run() {
						if (LOG.isInfoEnabled()) {
							LOG.info("save to db START");
						}
						Configuration configuration = new Configuration().configure();
						SchemaExport export = new SchemaExport(configuration);
						export.create(false, true);

						Session session = HibernateUtil.getSessionFactory().openSession();
						Transaction tx = null;
						try {
							tx = session.beginTransaction();
							for (Movie movie : movies) {
								movie.setId((int) session.save(movie));
							}
							tx.commit();
						} catch (HibernateException e) {
							if (tx != null) {
								tx.rollback();
							}
						} finally {
							session.close();
						}
						if (LOG.isInfoEnabled()) {
							LOG.info("save to db END");
						}
					}
				}.start();

				fillGridPane();

				if (LOG.isInfoEnabled()) {
					LOG.info("update END");
				}
			}
		};
		thread.setDaemon(true);
		thread.start();
	}

	/**
	 * set the number of movies to the label "Anzahl"
	 */
	private void setUpdateAndNumberMoviesLbl() {
		movieCountLbl.setText(String.valueOf(movies.size()));
		updateLbl.setText(PreferencesStore.getLastCheck());
	}

	/**
	 * disable the update button and shows a progressbar
	 * 
	 * @param disable
	 *            True if button should be disabled and progressbar should be visible, other wise false;
	 */
	private void setUpdateBtnDisable(Boolean disable) {
		updateBtn.setDisable(disable);
		loadProgress.setVisible(disable);
		if (disable) {
			loadProgress.setProgress(-1.0f);
		} else {
			loadProgress.setProgress(0);
		}
	}

	/**
	 * fill the MovieLayout.fxml and add it to the GridPane
	 */
	private void fillGridPane() {
		Platform.runLater(() -> {
			gridPane.getChildren().clear();
		});

		MainLayoutController mlc = this;

		Thread thread = new Thread("fill gridPane") {
			@Override
			public void run() {
				try {
					int colIndex = 0;
					int rowIndex = 0;
					int colCount = (int) (movieTab.getWidth() / 145) - 1;

					for (Movie movie : movies) {
						FXMLLoader loader = new FXMLLoader();
						loader.setLocation(Main.class.getResource("view/MovieLayout.fxml"));
						AnchorPane movieLayout = (AnchorPane) loader.load();

						MovieLayoutController controller = loader.getController();
						controller.setMovie(movie);
						controller.setImage();
						controller.setMainLayoutController(mlc);

						int cIndex = colIndex;
						int rIndex = rowIndex;

						Platform.runLater(() -> {
							gridPane.add(movieLayout, cIndex, rIndex);
						});
						if (colIndex < colCount) {
							colIndex++;
						} else {
							colIndex = 0;
							rowIndex++;
						}
					}
				} catch (IOException e) {
					if (LOG.isErrorEnabled()) {
						LOG.error("coudn't load fxml", e);
					}
				}

				Platform.runLater(() -> {
					setUpdateBtnDisable(false);
					setUpdateAndNumberMoviesLbl();
				});
			}
		};
		thread.setDaemon(true);
		thread.start();
	}

	@FXML
	private void handleReset() {
		importMovies = copySet;
		copySet = new TreeSet<>();
		selMovie();
	}

	@FXML
	private void handleImport() {
		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("bitte w�hlen sie das file zum importieren");
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TXT", "*.txt"),
				new FileChooser.ExtensionFilter("All Images", "*.*"));
		File importFile = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

		try (BufferedReader br = Files.newBufferedReader(importFile.toPath(), StandardCharsets.UTF_8)) {
			for (String line; (line = br.readLine()) != null;) {
				importMovies.add(line);
			}
		} catch (FileNotFoundException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("file not found " + importFile.getAbsolutePath());
			}
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("error while reading file: " + importFile.getAbsolutePath(), e);
			}
		}
		selMovie();
	}

	public void selMovie() {
		final int LABEL = 0;
		final int BUTTON = 3;
		
		ObservableList<Node> girdList = gridPane.getChildren();
		int pos = 0;
		for (String selMovie : importMovies) {
			for (int i = pos; i < girdList.size(); i++) {
				AnchorPane gridMovie = (AnchorPane) girdList.get(i);
				Label lbl = (Label) gridMovie.getChildren().get(LABEL);
				selMovie = selMovie.replaceAll("\\w:\\\\[A-Za-z-09-]+\\\\|\\.[0-9]+p.mkv", "");
				if (lbl.getText().startsWith(selMovie) || selMovie.startsWith(lbl.getText())) {
					Button btn = (Button) gridMovie.getChildren().get(BUTTON);
					btn.fire();
					pos = i;
					break;
				}
			}
		}
	}

	@FXML
	private void handleCopy() {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("choose target path");
		File target = chooser.showDialog(mainApp.getPrimaryStage());

		cdc = mainApp.showCopyDialog(copySet.size());

		Thread copy = new Thread(new Runnable() {
			@Override
			public void run() {
				for (String path : copySet) {
					Path source = Paths.get(path);
					try {
						Platform.runLater(() -> {
							cdc.incActMovie();
						});
						Files.copy(source, target.toPath().resolve(source.getFileName()), ExtendedCopyOption.INTERRUPTIBLE);
					} catch (IOException e) {
						if (LOG.isErrorEnabled())
							LOG.error("coudn't copy " + path + " to " + target, e);
					}
				}
				if (!Thread.interrupted()) {
					Platform.runLater(() -> {
						cdc.handleCancel();
					});
				}
			}
		}, "copy");
		copy.setDaemon(true);
		copy.start();

		Thread checkDialogCancel = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					if (cdc.isCancelClicked()) {
						copy.interrupt();
						break;
					}
				}
			}
		}, "dialog cancel");
		checkDialogCancel.setDaemon(true);
		checkDialogCancel.start();

		//
		// if (LOG.isDebugEnabled()) {
		// LOG.debug("target path: " + target);
		// LOG.debug("copyset:");
		// for (String path : copySet) {
		// LOG.debug(path);
		// }
		// }
	}

	@FXML
	private void handleExit() {
		Platform.exit();
	}

	public void setMainApp(Main main) {
		this.mainApp = main;
	}

	/**
	 * loads the database if exist
	 */
	@SuppressWarnings("unchecked")
	public void loadDB() {
		File database = new File("./res/movieinfo.mv.db");

		if (database.exists() && !database.isDirectory()) {
			if (LOG.isInfoEnabled()) {
				LOG.info("load movies from database START");
			}
			setUpdateBtnDisable(true);

			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				movies = new TreeSet<>(session.createCriteria(Movie.class).list());
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
			} finally {
				session.close();
			}
			if (LOG.isInfoEnabled()) {
				LOG.info("load movies from database END");
			}
			fillGridPane();
		}
	}

	/**
	 * set file path for selected movies to copy
	 * 
	 * @param path
	 *            file path
	 */
	public void addMovie(String path, double size) {
		size = size / 1024 / 1024 / 1024;
		copySet.add(path);
		if (copySet.size() == 1) {
			resetBtn.setDisable(false);
			copyBtn.setDisable(false);

			selMovieCountLbl.setVisible(true);
			selMovieCountText.setVisible(true);
			selMovieSizeLbl.setVisible(true);
			selMovieSizeText.setVisible(true);
		}
		selMovieCountText.setText(String.valueOf(copySet.size()));
		size += Double.parseDouble(selMovieSizeText.getText().replace(" GB", ""));
		selMovieSizeText.setText(String.valueOf(round(size, 2)) + " GB");
	}

	/**
	 * remove file path for selected movies to copy
	 * 
	 * @param path
	 *            file path
	 */
	public void removeMovie(String path, double size) {
		size = size / 1024 / 1024 / 1024;
		copySet.remove(path);
		if (copySet.isEmpty()) {
			resetBtn.setDisable(true);
			copyBtn.setDisable(true);

			selMovieCountLbl.setVisible(false);
			selMovieCountText.setVisible(false);
			selMovieSizeLbl.setVisible(false);
			selMovieSizeText.setVisible(false);
		}
		selMovieCountText.setText(String.valueOf(copySet.size()));
		size = Double.parseDouble(selMovieSizeText.getText().replace(" GB", "")) - size;
		selMovieSizeText.setText(String.valueOf(round(size, 2)) + " GB");
	}

	/**
	 * round the value
	 * 
	 * @param value
	 *            the value that should be rounded
	 * @param places
	 *            the places round to
	 * @return the rounded value
	 */
	private double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	/**
	 * set the movie info to the info tab
	 * 
	 * @param title
	 *            title of the movie
	 * @param year
	 *            year of the movie
	 * @param outline
	 *            outline text of the movie
	 * @param plot
	 *            plot text of the movie
	 */
	public void setMovieNameLbl(String title, String year, String outline, String plot) {
		movieInfoBox.setVisible(true);
		movieTitleLbl.setText(title);
		movieYearLbl.setText("(" + year + ")");
		Text outlineTxt = new Text(outline);
		outlineTxtFlow.getChildren().clear();
		outlineTxtFlow.getChildren().add(outlineTxt);
		Text plotTxt = new Text(plot);
		plotTxtFlow.getChildren().clear();
		plotTxtFlow.getChildren().add(plotTxt);

	}
}
