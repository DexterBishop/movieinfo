package de.bre.mi.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;

public class CopyDialogController {
	@FXML
	private Label movieLbl;

	@FXML
	private ProgressBar progressbar;

	private Stage dialogStage;
	private int numberMovies;
	private int actMovie = 0;
	private boolean cancelClicked = false;

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setNumberMovies(int numberMovies) {
		this.numberMovies = numberMovies;
		setText();
	}

	/**
	 * Returns true if the user clicked OK, false otherwise.
	 * 
	 * @return
	 */
	public boolean isCancelClicked() {
		return cancelClicked;
	}

	/**
	 * Called when the user clicks cancel.
	 */
	@FXML
	public void handleCancel() {
		cancelClicked = true;
		dialogStage.close();
	}

	/**
	 * increment actual number of copied movie
	 */
	public void incActMovie() {
		actMovie++;
		setText();
	}

	/**
	 * set the label text
	 */
	private void setText() {
		movieLbl.setText("Film " + actMovie + " von " + numberMovies);
	}
}
