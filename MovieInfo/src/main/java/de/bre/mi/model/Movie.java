package de.bre.mi.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Movie implements Comparable<Movie> {
	private int id; // NOPMD by dbg on 27.09.14 14:22
	private String path;
	private String fileName;
	private double fileSize;
	private String title;
	private String year;
	private String outline;
	private String plot;
	private String runtime;
	private String playcount;
	private String dateadded;
	private Fileinfo fileinfo;

	public Movie() {}

	public Movie(int id, String path, String fileName, double fileSize, String title, String year, String outline, String plot,
			String runtime, String playcount, String dateadded, Fileinfo fileinfo) {
		this.id = id;
		this.path = path;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.title = title;
		this.year = year;
		this.outline = outline;
		this.plot = plot;
		this.runtime = runtime;
		this.playcount = playcount;
		this.dateadded = dateadded;
		this.fileinfo = fileinfo;
	}

	@XmlTransient
	public int getId() { // NOPMD by dbg on 27.09.14 14:23
		return id;
	}

	public void setId(int id) { // NOPMD by dbg on 27.09.14 14:23
		this.id = id;
	}

	@XmlTransient
	public String getPath() { // NOPMD by dbg on 27.08.14 15:43
		return path;
	}

	public void setPath(String path) { // NOPMD by dbg on 27.08.14 15:43
		this.path = path;
	}

	@XmlTransient
	public String getFileName() { // NOPMD by dbg on 27.08.14 15:43
		return fileName;
	}

	@XmlTransient
	public double getFileSize() {
		return fileSize;
	}

	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}

	public void setFileName(String fileName) { // NOPMD by dbg on 27.08.14 15:43
		this.fileName = fileName;
	}

	public String getTitle() { // NOPMD by dbg on 27.08.14 15:43
		return title;
	}

	public void setTitle(String title) { // NOPMD by dbg on 27.08.14 15:43
		this.title = title;
	}

	public String getYear() { // NOPMD by dbg on 27.08.14 15:43
		return year;
	}

	public void setYear(String year) { // NOPMD by dbg on 27.08.14 15:43
		this.year = year;
	}

	public String getOutline() { // NOPMD by dbg on 27.08.14 15:43
		return outline;
	}

	public void setOutline(String outline) { // NOPMD by dbg on 27.08.14 15:43
		this.outline = outline;
	}

	public String getPlot() { // NOPMD by dbg on 27.08.14 15:43
		return plot;
	}

	public void setPlot(String plot) { // NOPMD by dbg on 27.08.14 15:43
		this.plot = plot;
	}

	public String getRuntime() { // NOPMD by dbg on 27.08.14 15:43
		return runtime;
	}

	public void setRuntime(String runtime) { // NOPMD by dbg on 27.08.14 15:43
		this.runtime = runtime;
	}

	public String getPlaycount() { // NOPMD by dbg on 27.08.14 15:43
		return playcount;
	}

	public void setPlaycount(String playcount) { // NOPMD by dbg on 27.08.14 15:43
		this.playcount = playcount;
	}

	public String getDateadded() { // NOPMD by dbg on 27.08.14 15:43
		return dateadded;
	}

	public void setDateadded(String dateadded) { // NOPMD by dbg on 27.08.14 15:43
		this.dateadded = dateadded;
	}

	public Fileinfo getFileinfo() {
		return fileinfo;
	}

	public void setFileinfo(Fileinfo fileinfo) {
		this.fileinfo = fileinfo;
	}

	/**
	 * if filename is equal compares year
	 */
	@Override
	public int compareTo(Movie movie) {
		if (this.fileName.compareTo(movie.fileName) == 0) {
			return this.year.compareTo(movie.year);
		} else {
			return this.fileName.compareTo(movie.fileName);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("id: " + id + "\npath: " + path + "\nfilename: " + fileName + "\nfilesize: " + fileSize
				+ "\ntitle: " + title + "\nyear: " + year + "\noutline: " + outline + "\nplot: " + plot + "\nruntime" + runtime
				+ "\nplaycount: " + playcount + "\ndateadded: " + dateadded + "\nfileinfo:\n");
		if (fileinfo != null) {
			sb.append(fileinfo.toString());
		}
		return sb.toString();
	}

}
