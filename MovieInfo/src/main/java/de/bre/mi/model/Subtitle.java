package de.bre.mi.model;

import javax.xml.bind.annotation.XmlTransient;

public class Subtitle {
	private int id;
	private String language;

	public Subtitle() {}

	public Subtitle(int id, String language) {
		this.id = id;
		this.language = language;
	}

	@XmlTransient
	public int getId() { // NOPMD by dbg on 27.09.14 14:23
		return id;
	}

	public void setId(int id) { // NOPMD by dbg on 27.09.14 14:23
		this.id = id;
	}
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String toString() {
		return "\n   id: " + id + "\n   language: " + language;
	}
}
