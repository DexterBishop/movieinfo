package de.bre.mi.model;

import javax.xml.bind.annotation.XmlTransient;

public class Stream {
	private int id;
	private String codec;
	private String bitrate;

	public Stream() {}

	public Stream(int id, String codec, String bitrate) {
		this.id = id;
		this.codec = codec;
		this.bitrate = bitrate;
	}

	@XmlTransient
	public int getId() { // NOPMD by dbg on 27.09.14 14:23
		return id;
	}

	public void setId(int id) { // NOPMD by dbg on 27.09.14 14:23
		this.id = id;
	}
	
	public String getCodec() {
		return codec;
	}

	public void setCodec(String codec) {
		this.codec = codec;
	}

	public String getBitrate() {
		return bitrate;
	}

	public void setBitrate(String bitrate) {
		this.bitrate = bitrate;
	}

	@Override
	public String toString() {
		return "\n   id: " + id + "\n   codec: " + codec + "\n   bitrate: " + bitrate;
	}
}
