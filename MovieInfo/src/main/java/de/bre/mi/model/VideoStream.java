package de.bre.mi.model;

import javax.xml.bind.annotation.XmlTransient;

public class VideoStream extends Stream {
	private int id;
	private String codec;
	private String bitrate;
	private String aspect;
	private String durationinseconds;
	private String height;
	private String width;

	public VideoStream() {
		super();
	}

	public VideoStream(int id, String codec, String bitrate, String aspect, String durationinseconds, String height, String width) {
		this.id = id;
		this.codec = codec;
		this.bitrate = bitrate;
		this.aspect = aspect;
		this.durationinseconds = durationinseconds;
		this.height = height;
		this.width = width;
	}

	@XmlTransient
	public int getId() { // NOPMD by dbg on 27.09.14 14:23
		return id;
	}

	public void setId(int id) { // NOPMD by dbg on 27.09.14 14:23
		this.id = id;
	}

	public String getCodec() {
		return codec;
	}

	public void setCodec(String codec) {
		this.codec = codec;
	}

	public String getBitrate() {
		return bitrate;
	}

	public void setBitrate(String bitrate) {
		this.bitrate = bitrate;
	}

	public String getAspect() {
		return aspect;
	}

	public void setAspect(String aspect) {
		this.aspect = aspect;
	}

	public String getDurationinseconds() {
		return durationinseconds;
	}

	public void setDurationinseconds(String durationinseconds) {
		this.durationinseconds = durationinseconds;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	@Override
	public String toString() {
		return "\n   id: " + id + "\n   codec: " + codec + "\n   bitrate: " + bitrate + "\n   aspect: " + aspect
				+ "\n   durationinseconds: " + durationinseconds + "\n   height: " + height + "\n   width:" + width;
	}
}
