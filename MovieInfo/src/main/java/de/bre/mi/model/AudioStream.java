package de.bre.mi.model;

import javax.xml.bind.annotation.XmlTransient;

public class AudioStream {
	private int id;
	private String codec;
	private String bitrate;
	private String channels;
	private String language;

	public AudioStream() {
		super();
	}

	public AudioStream(int id, String codec, String bitrate, String channels, String language) {
		this.id = id;
		this.codec = codec;
		this.bitrate = bitrate;
		this.channels = channels;
		this.language = language;
	}

	@XmlTransient
	public int getId() { // NOPMD by dbg on 27.09.14 14:23
		return id;
	}

	public void setId(int id) { // NOPMD by dbg on 27.09.14 14:23
		this.id = id;
	}

	public String getCodec() {
		return codec;
	}

	public void setCodec(String codec) {
		this.codec = codec;
	}

	public String getBitrate() {
		return bitrate;
	}

	public void setBitrate(String bitrate) {
		this.bitrate = bitrate;
	}

	public String getChannels() {
		return channels;
	}

	public void setChannels(String channels) {
		this.channels = channels;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String toString() {
		return "\n   id: " + id + "\n   codec: " + codec + "\n   bitrate: " + bitrate + "\n   channels: " + channels + "\n   language:"
				+ language;
	}
}
