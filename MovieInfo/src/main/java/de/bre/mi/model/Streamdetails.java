package de.bre.mi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

public class Streamdetails {
	private int id;
	private List<AudioStream> audio;
	private VideoStream video;
	private List<Subtitle> subtitle;

	public Streamdetails() {}

	public Streamdetails(int id, List<AudioStream> audio, VideoStream video, List<Subtitle> subtitle) {
		this.id = id;
		this.audio = audio;
		this.video = video;
		this.subtitle = subtitle;
	}

	@XmlTransient
	public int getId() { // NOPMD by dbg on 27.09.14 14:23
		return id;
	}

	public void setId(int id) { // NOPMD by dbg on 27.09.14 14:23
		this.id = id;
	}

	public List<AudioStream> getAudio() {
		return audio;
	}

	public void setAudio(List<AudioStream> audio) {
		this.audio = audio;
	}

	public VideoStream getVideo() {
		return video;
	}

	public void setVideo(VideoStream video) {
		this.video = video;
	}

	public List<Subtitle> getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(List<Subtitle> subtitle) {
		this.subtitle = subtitle;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("\n  id: " + id + "\n  audio:");
		if (audio != null) {
			for (AudioStream audioStream : audio) {
				sb.append(audioStream.toString());
			}
		}
		sb.append("\n  video: " + video + "\n  subtitle:");
		if (subtitle != null) {
			for (Subtitle sub : subtitle) {
				sb.append(sub.toString());
			}
		}
		return sb.toString();
	}
}
