package de.bre.mi.model;

import javax.xml.bind.annotation.XmlTransient;

public class Fileinfo {
	private int id;
	private Streamdetails streamdetails;

	public Fileinfo() {}

	public Fileinfo(int id, Streamdetails streamdetails) {
		this.id = id;
		this.streamdetails = streamdetails;
	}

	@XmlTransient
	public int getId() { // NOPMD by dbg on 27.09.14 14:23
		return id;
	}

	public void setId(int id) { // NOPMD by dbg on 27.09.14 14:23
		this.id = id;
	}

	public Streamdetails getStreamdetails() {
		return streamdetails;
	}

	public void setStreamdetails(Streamdetails streamdetails) {
		this.streamdetails = streamdetails;
	}

	@Override
	public String toString() {
		return "\n id: " + id + "\n streamdetails: " + streamdetails.toString();
	}
}
