package de.bre.mi;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bre.mi.view.CopyDialogController;
import de.bre.mi.view.MainLayoutController;

public class Main extends Application { // NOPMD by dbg on 27.08.14 15:35
	private static final Logger LOG = LoggerFactory.getLogger(Main.class);

	private Stage primaryStage;

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Movie v0.0.5");

		showMainLayout();
	}

	/**
	 * Shows the tab layout inside the root layout.
	 */
	public void showMainLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/MainLayout.fxml"));
			BorderPane mainLayout = (BorderPane) loader.load();

			MainLayoutController controller = loader.getController();
			controller.setMainApp(this);
			
			// Show the scene containing the root layout.
			Scene scene = new Scene(mainLayout);
			primaryStage.setScene(scene);
			primaryStage.setMaximized(true);
			primaryStage.show();
			controller.loadDB();
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("coudn't load fxml", e);
			}
		}
	}

	/**
	 * Shows the new/edit dialog.
	 * 
	 * @return is cancel clicked
	 */
	public CopyDialogController showCopyDialog(int numberMovies) {
		CopyDialogController controller = null;
		
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/CopyDialog.fxml"));
			VBox page = (VBox) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Copy");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.setResizable(false);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the wlentry into the controller.
			controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setNumberMovies(numberMovies);

			// Show the dialog and wait until the user closes it
			dialogStage.show();
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("coudn't load fxml", e);
			}
		}
		return controller;
	}
	
	public static void main(String[] args) { // NOPMD by dbg on 27.08.14 15:42
		launch(args);
	}
}
