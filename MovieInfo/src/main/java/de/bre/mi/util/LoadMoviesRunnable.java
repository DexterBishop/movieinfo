package de.bre.mi.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bre.mi.model.Movie;

/**
 * loads the NFO files in a directory and add the info to a Set
 * 
 * @author dbg
 *
 */
public class LoadMoviesRunnable implements Runnable {
	private static final Logger LOG = LoggerFactory.getLogger(LoadMoviesRunnable.class);

	private final String path;
	// movies Set from MainLayoutcontroler
	private final Set<Movie> movies;

	public LoadMoviesRunnable(String path, Set<Movie> movies) {
		super();
		this.path = path;
		this.movies = Collections.synchronizedSet(movies);
	}

	@Override
	public void run() {
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(path), "*.{nfo}");) {
			if (LOG.isInfoEnabled()) {
				LOG.info("read nfo's in " + path + " START");
			}
			for (Path filePath : stream) {

				// Files.walk(Paths.get(path)).forEach(filePath -> {
				String file = filePath.getFileName().toString();
				// if (Files.isRegularFile(filePath) && file.endsWith(".nfo")) {
				// if (LOG.isDebugEnabled()) {
				// LOG.debug(filePath.toString());
				// }
				try {
					JAXBContext context = JAXBContext.newInstance(Movie.class);
					Unmarshaller unmarshaller = context.createUnmarshaller();

					Movie movie = (Movie) unmarshaller.unmarshal(filePath.toFile());
					movie.setPath(filePath.getRoot().toString() + filePath.subpath(0, filePath.getNameCount() - 1));
					String fileNamer = file.replace(".nfo", "");
					movie.setFileName(fileNamer);
					movie.setFileSize(new File(path + "\\" + fileNamer + ".mkv").length());
					movies.add(movie);
				} catch (JAXBException e) {
					if (LOG.isErrorEnabled()) {
						LOG.error("coudn't load nfo: " + file, e);
					}
				}
			}
			// });
			if (LOG.isInfoEnabled()) {
				LOG.info("read nfo's in " + path + " END");
			}
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("coudn't load nfo", e);
			}
		}
	}

}
