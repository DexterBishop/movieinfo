package de.bre.mi.util;

import java.util.prefs.Preferences;

import de.bre.mi.Main;

public final class PreferencesStore {
	private static final String LAST_UPDATE = "lastUpdate";
	private static final Preferences PREFES = Preferences.userNodeForPackage(Main.class);

	private PreferencesStore() {}

	/**
	 * Load the date and time from the update from Preferences and give it as a String back.
	 * 
	 * @return returns the date and time from last RSS feed check as a String
	 */
	public static String getLastCheck() {
		String lastCheck = PREFES.get(LAST_UPDATE, null);
		if (lastCheck == null) {
			lastCheck = "";
		}
		return lastCheck;
	}

	/**
	 * Save the date and time from the update in to Preferences.
	 * 
	 * @param lastCheck
	 *            date and time as a String
	 */
	public static void setLastCheck(String lastCheck) {
		if (lastCheck == null) {
			PREFES.remove(LAST_UPDATE);
		} else {
			PREFES.put(LAST_UPDATE, lastCheck);
		}
	}
}
